/* Faça um programa que imprima na tela se um nome é igual ao outro nome digitado. Porém, os dois nomes devem estar em dois objetos separados. Ex:
const object = {name: "João"}, object1={name:"maria"}. João e João, imprime true. João e Maria, imprime false. Faça um programa que imprima na tela se um nome é igual ao outro nome digitado. Porém, os dois nomes devem esta em um objeto. Ex: const object1= {firstName:"João", seccondName:"Maria"}. João e João, imprime true. João e Maria, imprime false. */

const joão = {
    name: "João"
}

const maria = {
    name: "Maria"
}

console.log(joão.name === maria.name)