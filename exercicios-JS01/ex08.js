/* Faça um programa que imprima na tela se um nome é diferente ao outro nome digitado. Porem, os dois nomes devem estar em um Array.
Ex: const = ["João", "Maria"]. João e João, imprime fase. João e Maria, imprime true. */

const names = ["João", "João"]

const compare = names[0] !== names[1]

console.log(compare)