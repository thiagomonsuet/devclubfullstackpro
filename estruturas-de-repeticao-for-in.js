/* 
ESTRUTURAS DE REPETIÇÃO FOR IN
 */

const student = {
    name:"Thiago",
    age:36,
    genre:"male"
}

/* console.log(student.name) */

for(let property in student){
    console.log(`${property}:${student[property]}`)
}