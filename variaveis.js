// VARIÁVEIS

var number1 = 1; // NÃO USEM O VAR, 
let number2 = 2;
const number3 = 3; //constante nunca pode ser mudado

console.log(number1);
console.log(number2);
console.log(number3);

number1 = 10;
number2 = 20;
number3 = 30;
/**
 * PERMITIDO:
 * 
 * iNICIAR COM $ OU _
 * Colocar acentos
 * Inciar com letras
 * Letras maísculas e minúsculas
 * 
 * NÃO PODE:
 * 
 * IDEAL
 * 
 * Camel case: euEscrevoAssim
 * snake case: eu_vou_escrever_assim
 * SEMPRE em ingles
 * nomes que fazem sentido
 */