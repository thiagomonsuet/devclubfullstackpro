/** STRING
 * 
 * - Cadeia de Caracteres
 */

let myFirstString = "E aqui dentro eu posso escrever o que eu queser!"
let mySeccondString = 'Agora com aspas simples'
let myThirdString = `Com a crase eu tambem consigo` // template literals ou template strings

let numberOfPeopleInClass = 34
let myTextWithSuperPowers = `Tinham exatamente ${numberOfPeopleInClass} pessoas na aula`
console.log(myTextWithSuperPowers)