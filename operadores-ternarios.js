/* OPERADOR TERNÁRIO OU CONDICIONAL

? se
: se não
*/

const rain = false
const umbrela = rain ? 'Vou levar meu guarda-chuva' : 'Vou deixar meu guarda-chuva em casa.'

/* console.log(umbrela) */


// PRECISO TRANSFERIR MEU DINHEIRO

const balance = false
const isNotBlocked = true
const accountExist = true

const transferOk = balance && isNotBlocked && accountExist ? 'TRANSFERENCIA REALIADA COM SUCESSO' : 'SUA TRANSFERENCIA FOI NEGADA'

console.log(transferOk)

