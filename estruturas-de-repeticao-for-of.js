/* 
ESTRUTURAS DE REPETIÇÃO FOR OF
*/

let myName = 'Thiago'
let allName = ['Maria', 'João', 'Pedro', 'Gilberto']

/* for(let letter of myName){
    console.log(letter)
} */

for(let name of allName){
    console.log(name)
}