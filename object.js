/**OBJECT
 * 
 * Propriedade: valor
 * marca: Samsung
 * cor: preta
 * tamanho: 75
 * 
 * nome: Thiago
 * idade: 36
 * altura: 1.7m
 */

const person = { // formato do objeto 
    name: "Thiago",
    age: 36,
    sex: "male",
    height: 1.7,
    address: {
        street: "AV Central",
        set: 1,
        house: 17,
        country: "Brasil"
    }
}

console.log(person.address.street)
