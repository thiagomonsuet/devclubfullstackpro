/*VAMOS PRATICAR
  
- Por que fazer esses exercícios ? Agora é a hora de treinar seu cérebro a como "conversar com o computador".
- Você precisa desenvolver uma lógica forte, para enfrentar qualquer desafio.
- Desenvolver raciocínio lógico.

[ok] Faça um programa que SOME 2 números
[ok] Faça um programa que multiplique 2 números e o resultado adicione 10
[ok] Faça um programa que encontre a raiz quadrada de um número, multiplique o resultado por 10 e divida por 33
[ok] Faça um programa que iniciei com dois nomes, e o programa imprima na tela o seguinte dado: Olá, meu nome é (nome1) e meu partner do CodeClub é (nome2)

[ok] Faça um programa que imprima na tela se um nome é igual ao outro nome digitado: Ex-> João e João, imprime true. João e Maria, imprime false.

[ok] Faça um programa que imprima na tela se um nome é diferente ao outro nome digitado. 
Ex: João e João, imprime false. João e Maria, imprime true.

[ok] Faça um programa que imprima na tela se um nome é igual ao outro nome digitado. Porem, os dois nomes devm estar em um Array. Ex: const array = ["João", "Maria"]. João e João, imprime true. João e Maria, imprime false.

[ok] Faça um programa que imprima na tela se um nome é diferente ao outro nome digitado. Porem, os dois nomes devem estar devem estar em um Array.
Ex: const = ["João", "Maria"]. João e João, imprime fase. João e Maria, imprime true.

[ok] Crie 5 Objetos, neles devem conter os dados de 5 pessoas que você conhece. Mínimo 5 dados por pessoa.

[ok] Faça um programa que imprima na tela se um nome é igual ao outro nome digitado. Porém, os dois nomes devem estar em dois objetos separados. Ex:
const object = {name: "João"}, object1={name:"maria"}. João e João, imprime true. João e Maria, imprime false. 

[ok] Faça um progrma onde toda vez que ele rodar coloca na tela um número aleatorio entre 1 e 100.

[ok] Faça um programa onde colocamos dois numeros ímpares e o programa imprima o resto da divisão. Ex: 7/3 deve imprimir: 1,25/25 deve imprimir:0

[ok] Faça um progrma onde entramos com dois números, e ele imprime se o primeiro número é maior que o segundo. Ex: 2 e 5. Imprime: false.

[ok] Faça um progrma onde entramos com dois números, e ele imprime se o primeiro número é menor que o segundo. Ex: 2 e 5. Imprime true.

[ok] Crie um programa que mostra o tamanho do Array. Ex const array = [0,1]
imprime: 2. const array = ["banana", "Maça", "laranja"] imprime 3
*/