/*  OPERADORES DE ATRIBUIÇÃO 

=   -> Atribuição
+=  -> Adição
-=  -> Subtração
/=  -> Divição
*=  -> Multiplicação
%=  -> Resto 
*/

let firstNumber = 10

//firstNumber = firstNumber + 2
firstNumber /= 2 // firstNumber seja o valor de firstNumber + 2

console.log(firstNumber)