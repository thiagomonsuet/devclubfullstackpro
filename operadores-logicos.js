/* OPERADORES aritméticos 
   
+  -> Adição
-  -> Subtração
*  -> Multiplicação
/  -> Divição
%  -> Resto
++ -> Incremento
-- -> Decremento
** -> Exponencial


myNumber = 10
myNumber++
myNumber++
myNumber++

console.log(20 + 30)
console.log(30 - 20)
console.log(20 * 30)
console.log(20 / 2)
console.log(30 % 7)
console.log(++myNumber)
console.log(2 ** 3)*/


/* OPERADORES LÓGICOS

&&  -> E 
    true && true = true
    true && false = false
    false && false = false

||  -> Ou
    true || true = true
    true || false = true
    false || false = false

!   -> Negação
    !true = false
    !false = true

    O que é necessário para alguém fazer uma transferéncia bancária?

    -Ter saldo em conta
    -Minha conta não esteja bloqueada
    -A conta destino precisa estar correta
*/


