/* CONTROLE DE FLUXO - CONDICIONAIS - IF / ELSE

if = se
else = se não
*/

const age = 17

if(age >= 18){
    console.log('Você é maior de idade ')
}else{
    console.log('Você não é maior de idade')
}